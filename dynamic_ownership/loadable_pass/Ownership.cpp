#define DEBUG_TYPE "ownership"
#include "llvm/Module.h"
#include "llvm/Pass.h"
#include "llvm/Function.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/Support/InstIterator.h"
#include "llvm/Instructions.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include "llvm/DerivedTypes.h"
#include "llvm/LLVMContext.h"

using namespace llvm;

namespace {
  struct Ownership: public ModulePass {
    static char ID;
    bool foundMain;
    Ownership() : ModulePass(ID), foundMain(false) {}
    bool runOnFunction(Function &F, Module::PointerSize ps);
    virtual bool runOnModule(Module &M);
    void buildNewMain(Function &oldMain, Module &mod);
    
    };
}

bool Ownership::runOnModule(Module &M) {
    bool modified = false;

    for( Module::iterator it = M.begin(); it != M.end(); ++it) {
        Function    &f = *it;

        modified |= this->runOnFunction(f, M.getPointerSize());
    }
    return modified;
}

//this routine is called for every function in a module that is being compiled
//it returns true if the function was modified (it almost always is)
//
bool Ownership::runOnFunction( Function &F, Module::PointerSize ps ) {
    bool    modified = false;
    Module  *M = F.getParent();

    assert(ps == Module::Pointer32 || ps == Module::Pointer64);

    FunctionType  *tidFT;

    if( ps == Module::Pointer32 ) {
        tidFT = FunctionType::get( Type::getInt32Ty(M->getContext()), false);
    } else if( ps == Module::Pointer64 ) {
        tidFT = FunctionType::get( Type::getInt64Ty(M->getContext()), false);
    }

    Constant    *getTidFun = M->getOrInsertFunction("__gettid", tidFT);

    //build up the type array for the __checkMemOwnerR and __checkMemOwnerW
    //functions
    std::vector<Type *>   argTypes;
    if( ps == Module::Pointer32 ) {
        argTypes.push_back(Type::getInt32Ty(M->getContext()));
        argTypes.push_back(Type::getInt32Ty(M->getContext()));
    } else if( ps == Module::Pointer64 ) {
        argTypes.push_back(Type::getInt64Ty(M->getContext()));
        argTypes.push_back(Type::getInt64Ty(M->getContext()));
    }

    //type-signature of the __checkMemOwnerR and __checkMemOwnerW functions
    FunctionType    *FT = 
        FunctionType::get( Type::getVoidTy(M->getContext()), argTypes, false);

    //get a pointer to the library function that does the check
    Constant    *CheckFunR = M->getOrInsertFunction("__checkMemOwnerR", FT);
    Constant    *CheckFunW = M->getOrInsertFunction("__checkMemOwnerW", FT);

    //build up the type array for the call to ownMalloc
    std::vector<Type *>   argOwnMalloc;
    if( ps == Module::Pointer32 ) {
        argOwnMalloc.push_back(Type::getInt32Ty(M->getContext()));
        argOwnMalloc.push_back(Type::getInt32Ty(M->getContext()));
    } else if( ps == Module::Pointer64 ) {
        argOwnMalloc.push_back(Type::getInt64Ty(M->getContext()));
        argOwnMalloc.push_back(Type::getInt64Ty(M->getContext()));
    }

    //build up the type array for the call to ownFree 
    std::vector<Type *>   argOwnFree;
    if( ps == Module::Pointer32 ) {
        argOwnFree.push_back(Type::getInt32Ty(M->getContext()));
    } else if( ps == Module::Pointer64 ) {
        argOwnFree.push_back(Type::getInt64Ty(M->getContext()));
    }

    //get function types for external calls to ownMalloc and ownFree
    FunctionType    *FTMallocWrap = 
        FunctionType::get( Type::getVoidTy( M->getContext()), 
                                            argOwnMalloc, 
                                            false);
    FunctionType    *FTFreeWrap = 
        FunctionType::get( Type::getVoidTy( M->getContext()), 
                                            argOwnFree, 
                                            false);

    //get a pointer to the library function that does the check
    Constant *OwnMalloc = M->getOrInsertFunction("__ownMalloc", FTMallocWrap);
    Constant *OwnFree = M->getOrInsertFunction("__ownFree", FTFreeWrap);
       
    CallInst    *callCheckFn = NULL;
    //then, iterate over all the instructions in the function
    for(inst_iterator I = inst_begin(F), E = inst_end(F); I != E; ++I) {
        Instruction &inst = *I;

        //insert the call to __gettid exactly once, as the first instruction
        //in this function
        if( !callCheckFn) {
            callCheckFn = CallInst::Create(getTidFun, "", &inst); 
        }

        //if an instruction is a load or store, instrument it 
        LoadInst    *load;
        StoreInst   *store;
        Value       *memAddrWrite = NULL;
        Value       *memAddrRead = NULL;

        //test and see if the current instruction is a load or a store
        if( ( load = dyn_cast<LoadInst>(&inst) )) {
            memAddrRead = load->getOperand(0);
        } else if( ( store = dyn_cast<StoreInst>(&inst) ) ) {
            memAddrWrite = store->getOperand(1);
        }

        //if it is a read, instrument it by inserting a call to the runtime
        if( memAddrRead ) {
            Value   *v[2];
            v[1] = callCheckFn;

            //create a call instruction
            CastInst    *loadAddr;
            if( ps == Module::Pointer32 ) {
                loadAddr = CastInst::CreatePointerCast(memAddrRead,
                                            Type::getInt32Ty(M->getContext()),
                                            "",
                                            &inst);
            } else if( ps == Module::Pointer64 ) {
                loadAddr = CastInst::CreatePointerCast(memAddrRead,
                                            Type::getInt64Ty(M->getContext()),
                                            "",
                                            &inst);
            }
            v[0] = loadAddr;
            CallInst::Create(CheckFunR, v, "", &inst);

            modified = true;
        }
       
        //also if it is a write, instrument it by inserting a call to the rt
        if( memAddrWrite ) {
            Value   *v[2];
            v[1] = callCheckFn;

            //create a call instruction
            CastInst    *loadAddr;
            if( ps == Module::Pointer32 ) {
                loadAddr = CastInst::CreatePointerCast(memAddrWrite,
                                            Type::getInt32Ty(M->getContext()),
                                            "",
                                            &inst);
            } else if( ps == Module::Pointer64 ) {
                loadAddr = CastInst::CreatePointerCast(memAddrWrite,
                                            Type::getInt64Ty(M->getContext()),
                                            "",
                                            &inst);
            }
            v[0] = loadAddr;
            CallInst::Create(CheckFunW, v, "", &inst);

            modified = true;
        }

        //if the function is a call instruction, inspect its signature
        CallInst    *call;
        if( ( call = dyn_cast<CallInst>(&inst) ) ) {
            //TODO: if the signature containts any pointer types, 
            //      instrument them. this is to catch things like 
            //      memcpy, and we don't do this right now 
            Function    *f = call->getCalledFunction();

            if( f != NULL ) {
                //if this is a direct call and the target is malloc or free...
                if( f->getName() == "malloc" ) {
                    //errs() << "found malloc\n";
                    //this is a call to malloc, insert instrumentation AFTER
                    //the call to own the memory returned by malloc
                    Value   *v[3];
                    v[1] = call->getOperand(0);
                    inst_iterator   ne = I;
                    ne++;
                    if( ne != E ) {
                        Instruction &nextInst = *ne;
                        CastInst *loadAddr;
                        if( ps == Module::Pointer32 ) {
                            loadAddr = CastInst::CreatePointerCast(
                                            call,
                                            Type::getInt32Ty(M->getContext()),
                                            "",
                                            &nextInst);
                        } else if ( ps == Module::Pointer64 ) {
                            loadAddr = CastInst::CreatePointerCast(
                                            call,
                                            Type::getInt64Ty(M->getContext()),
                                            "",
                                            &nextInst);

                        }
                        v[0] = loadAddr;
                        CallInst::Create(OwnMalloc, v, "", &nextInst);
                        modified = true;
                    } else {
                        //ugh, freak out time 
                        //(we hit a case where the call was the last ins 
                        //in a block)
                        //shouldn't happen because in the IL a block should 
                        //end in a branch or return
                    }
                } else if( f->getName() == "free" ) {
                    //errs() << "found free\n";
                    //this is a call to free, insert instrumentation BEFORE
                    //the call to free to release the memory pointed to 
                    //and check/ensure that it is memory this thread owns
                    Value   *v[1];

                    CastInst *loadAddr;
                    if( ps == Module::Pointer32 ) {
                        loadAddr = CastInst::CreatePointerCast(
                                            call->getOperand(0),
                                            Type::getInt32Ty(M->getContext()),
                                            "",
                                            &inst);
                    } else if( ps == Module::Pointer64 ) {
                        loadAddr = CastInst::CreatePointerCast(
                                            call->getOperand(0),
                                            Type::getInt64Ty(M->getContext()),
                                            "",
                                            &inst);

                    }
                    v[0] = loadAddr;
                    CallInst::Create(OwnFree, v, "", &inst);
                    modified = true;
                }
            }
        }
    }
    
    return modified;
}

char Ownership::ID = 0;
static RegisterPass<Ownership>
Y("ownership", "ownership pass");
