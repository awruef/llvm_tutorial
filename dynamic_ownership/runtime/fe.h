typedef void * Cluster;

Cluster allocCluster(void);
void giveToCluster(void *, unsigned long, Cluster);
void ownClusterEx(void *);
void relClusterEx(void *);
void ownEx(void *, unsigned long);
void relEx(void *, unsigned long);
void makeRO(void *, unsigned long);
