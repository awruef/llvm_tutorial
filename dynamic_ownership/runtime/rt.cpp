#include "rt_cmn.h"
extern "C" {
#include "fe.h"
}

using namespace std;
using namespace boost;
using namespace boost::icl;

addrToDescT memToOwn;
mutex       ownLock;

extern "C" ptrdiff_t __gettid(void) {
    return (ptrdiff_t)(THREADID());
}

extern "C" void ownEx_locked(void *addr, unsigned long len) {
    ptrdiff_t tid = __gettid();
    ptrdiff_t lo = ((ptrdiff_t)addr);
    ptrdiff_t hi = lo+len;
   
    addrToDescT::const_iterator  it = 
        memToOwn.find(interval<ptrdiff_t>::closed(lo, hi));
    if( it != memToOwn.end() ) {
        AddrDesc n = (*it).second; 
        //if we're marking ownership, check and make sure we're not
        //stepping on something else
        if( tid != n.getTid() && n.getState() != Unowned ) {
            cout << "Taking ownership of something we don't own" << endl;
            cout << "owned by " << n.getTid() << endl;
            cout << "addr " << hex << lo << dec << endl;
            cout << "state is " << n.getState() << endl;
            cout << (*it).first << endl;
        } else {
            n.setOwned(tid);
            memToOwn.add(make_pair(interval<ptrdiff_t>::closed(lo, hi), n));
        }
    } else {
        AddrDesc a(Owned, tid);
        //if we don't find this, we're okay
        memToOwn.add(make_pair(interval<ptrdiff_t>::closed(lo, hi), a));
    }

    return;
}


extern "C" Cluster allocCluster(void) {
#if 0
    return new clusterVecT();
#endif
    return NULL;
}

extern "C" void giveToCluster(void *p, unsigned long len, Cluster c) {
#if 0
    clusterVecT *cluster = (clusterVecT *)c;
    mutex::scoped_lock  l(ownLock);

    if( l ) {
        ptrdiff_t addr = (ptrdiff_t)p;
        cluster->push_back(rangeT(p, len));
        ownEx_locked(p, len);
        addrToDescT::const_iterator it = 
            memToOwn.find(interval<ptrdiff_t>::closed(addr, addr+len));
        if( it == memToOwn.end() ) {
            cout << "The impossible! we just added this!" << endl;
        } else {
            AddrDesc adt = (*it).second;
            adt.cluster = cluster;
            memToOwn.add(make_pair((*it).first, adt));
        }
    }
#endif
}

extern "C" void ownClusterEx(void *p) {
#if 0
    //look up the address descriptor for the supplied ptr
    ptrdiff_t addr = ((ptrdiff_t)p);
    mutex::scoped_lock  l(ownLock);

    if( l ) {
        addrToDescT::const_iterator it = 
            memToOwn.find(interval<ptrdiff_t>::closed(addr, addr+1));
        if( it != memToOwn.end() ) {
            AddrDesc adt = (*it).second;
            clusterVecT *cluster = adt.cluster;
            if( cluster ) { 
                clusterVecT::iterator  viter = cluster->begin();
                while( viter != cluster->end() ) {
                    rangeT  r = *viter;
                    ownEx_locked(r.first, r.second);
                    
                    ++viter;
                }
            } else {
                cout << "There is no cluster for this address" << endl;
            }
        } else {
            cout << "Attempt to own cluster for address not in map!" << endl;
        }
    }
#endif
    return;
}

extern "C" void relEx_locked(void *addr, unsigned long len) {
    ptrdiff_t tid = __gettid();
    ptrdiff_t lo = ((ptrdiff_t)addr);
    ptrdiff_t hi = lo+len;

    addrToDescT::const_iterator  it = 
        memToOwn.find(interval<ptrdiff_t>::closed(lo, hi));
    if( it != memToOwn.end() ) {
        AddrDesc p = (*it).second;
        if( tid == p.getTid() ) {
            /*cout << "releasing... ";
            cout << interval<unsigned long>::closed(lo, hi) << endl;*/
            p.setUnowned();
            memToOwn.add(make_pair(interval<ptrdiff_t>::closed(lo,hi), p));
        } else {
            cout << "attempt to unown memory by a different thread" << endl;
        }
    } else {
        cout << "We don't own that range!";
    }
    return;
}

extern "C" void relClusterEx(void *p) {
#if 0
    //look up the address descriptor for the supplied ptr
    ptrdiff_t addr = ((ptrdiff_t)p);
    mutex::scoped_lock  l(ownLock);

    if( l ) {
        addrToDescT::const_iterator it = 
            memToOwn.find(interval<ptrdiff_t>::closed(addr, addr+1));
        if( it != memToOwn.end() ) {
            AddrDesc adt = (*it).second;
            clusterVecT *cluster = adt.cluster;
            if( cluster ) { 
                clusterVecT::iterator  viter = cluster->begin();
                while( viter != cluster->end() ) {
                    rangeT  r = *viter;
                    relEx_locked(r.first, r.second);
                    
                    ++viter;
                }
            } else {
                cout << "There is no cluster for this address" << endl;
            }
        } else {
            cout << "Attempt to release cluster for address not in map!" << endl;
        }
    }
#endif
    return;
}

extern "C" void ownEx(void *addr, unsigned long len) {
    mutex::scoped_lock  l(ownLock);

    if( l ) {
        ownEx_locked(addr, len);
    }

    return;
}

extern "C" void relEx(void *addr, unsigned long len) {
    mutex::scoped_lock  l(ownLock);

    if( l ) {
        relEx_locked(addr, len);
    }

    return;
}

void makeRO(void *addr, unsigned long len) {
#if 0
    ptrdiff_t tid = __gettid();
    ptrdiff_t lo = ((ptrdiff_t)addr);
    ptrdiff_t hi = lo+len;
    mutex::scoped_lock  l(ownLock);

    if( l ) {
        addrToDescT::const_iterator it = 
            memToOwn.find(interval<ptrdiff_t>::closed(lo, hi));
       
        if( it != memToOwn.end() ) {
            //do .. something?
            while( it != memToOwn.end() ) {
                AddrDesc o = (*it).second;
                //check and make sure that all these ranges are owned by us
                if( o.getTid() != tid ) {
                    cout << "owned by another thread: " << o.getTid() << endl;
                } else {
                    o.setRO();
                    memToOwn.add(
                        make_pair(interval<ptrdiff_t>::closed(lo, hi), o));
                }

                ++it;
            }
        } else {
            AddrDesc a(OwnedRO, tid);

            memToOwn.add(make_pair(interval<ptrdiff_t>::closed(lo, hi), a));
        }
    }
#endif
    return;
}

//TODO: needs access length
extern "C" void __checkMemOwnerR(void *addr, unsigned long tid) {
#if 0
    ptrdiff_t lo = ((ptrdiff_t)addr);
    ptrdiff_t hi = lo+1;
    mutex::scoped_lock  l(ownLock);

    if( l ) {
        addrToDescT::const_iterator it = 
            memToOwn.find(interval<ptrdiff_t>::closed(lo, hi));
        if( it != memToOwn.end() ) {
            AddrDesc owner = (*it).second;
            if( owner.getTid() != tid && owner.getState() != Unowned ) {
                if( owner.getState() != OwnedRO ) {
                    cout << "owned by another thread: " << owner.getTid() << endl;
                }
            }
        } else {
            cout << "R: no owner" << endl;
        }
    }
#endif
    return;
}

extern "C" void __checkMemOwnerW(void *addr, unsigned long tid) {
#if 0
    ptrdiff_t lo = ((ptrdiff_t)addr);
    ptrdiff_t hi = lo+1;
    mutex::scoped_lock  l(ownLock);

    if( l ) {
        addrToDescT::const_iterator it = 
            memToOwn.find(interval<ptrdiff_t>::closed(lo, hi));
        if( it != memToOwn.end() ) {
            AddrDesc owner = (*it).second;

            if( owner.getTid() != tid && owner.getState() != Unowned ) {
                cout << "owned by another thread: " << owner.getTid() << endl;
            }
        } else {
            cout << "W: no owner" << endl;
        }
    }
#endif
    return;
}


extern "C" void __attribute__((regparm(2))) __ownCurStack(void *stackBase, unsigned long disp) {
#if 0
    ptrdiff_t stackBottom = ((ptrdiff_t)stackBase)-disp;
    ownEx(((void*)stackBottom), disp);
#endif
    return;
}

extern "C" void __attribute__((regparm(2))) __relCurStack(void *stackBase, unsigned long disp) {
#if 0
    ptrdiff_t stackBottom = (((ptrdiff_t)stackBase)+sizeof(void*))-disp;
    relEx(((void*)stackBottom), disp);
#endif
    return;
}

extern "C" void __ownMalloc(void *mem, size_t len) {
#if 0
    ownEx(mem, len);
#endif
    return;
}

extern "C" void __ownFree(void *mem) {
#if 0
    ptrdiff_t a = ((ptrdiff_t)mem);
    mutex::scoped_lock  l(ownLock);
    
    if( l ) {
        addrToDescT::const_iterator  it = 
            memToOwn.find(boost::icl::interval<ptrdiff_t>::closed(a, a+1));

        if( it != memToOwn.end() ) {
            boost::icl::discrete_interval<ptrdiff_t, less> l = (*it).first; 
            ptrdiff_t low = l.lower();
            ptrdiff_t len = l.upper()-low;
            relEx_locked(((void*)low), len);
        } else {
            cout << "We don't have a mapping for this call to free" << endl; 
        }
    }
   #endif 
    return;
}
