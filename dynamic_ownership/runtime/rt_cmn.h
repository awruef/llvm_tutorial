#include <vector>
#include <iostream>

#include <boost/thread/mutex.hpp>
#include <boost/icl/interval_map.hpp>
#include <boost/icl/split_interval_map.hpp>

enum AddrState {
    Owned,
    Unowned,
    OwnedRO
};

typedef std::pair<void *, unsigned long> rangeT;
typedef std::vector<rangeT> clusterVecT;

class AddrDesc;

class AddrDesc {
private:
    AddrState               state;
    pthread_t               tid;
public:
    clusterVecT             *cluster;
    AddrDesc() : state(Unowned), tid(0), cluster(NULL) { return; }
    AddrDesc(AddrState s, ptrdiff_t t) : state(s), cluster(NULL) { 
        this->tid = (pthread_t) t; 
        return; 
    }
    
    AddrDesc operator=(const AddrDesc &rhs) {
        this->cluster = rhs.cluster;
        this->state = rhs.state;
        this->tid = rhs.tid;
        return *this;
    }

    AddrDesc operator+=(const AddrDesc &rhs) {
        this->cluster = rhs.cluster;
        this->state = rhs.state;
        this->tid = rhs.tid;
        return *this;
    }

    bool operator==(const AddrDesc &rhs) const {
        return false;
    }

    ptrdiff_t getTid(void) { return (ptrdiff_t) this->tid; }
    AddrState getState(void) { return this->state; }
    void setUnowned(void) { this->state = Unowned; this->tid = 0; }
    void setOwned(ptrdiff_t tid) { this->state = Owned; this->tid = (pthread_t) tid; }
    void setRO(void) { this->state = OwnedRO; }
};

typedef boost::icl::interval_map<ptrdiff_t, AddrDesc>  addrToDescT;

#ifdef WIN32
#else
#include <pthread.h>
#define THREADID() pthread_self()
#endif

/*typedef std::pair<unsigned long long, unsigned long long>   range;
typedef std::pair<range, unsigned long> rangeOwner;
typedef std::vector<rangeOwner> OwnerVector;

extern OwnerVector  g_OwnVec;*/

#ifdef WIN32
#else
#define BREAK() asm("int $0x3")
#endif
