#include "llvm/LLVMContext.h"
#include "llvm/DataLayout.h"
#include "llvm/DebugInfo.h"
#include "llvm/Module.h"
#include "llvm/PassManager.h"
#include "llvm/Support/IRReader.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/LinkAllPasses.h"
#include "llvm/LinkAllVMCore.h"
#include "escaped_allocas_pass.h"
#include <memory>
#include <algorithm>
using namespace llvm;
using namespace std;

static cl::opt<string>
InputFile(cl::Positional, cl::desc("input"));

int main(int argc, char *argv[])
{
  /* command line stuff */
  cl::ParseCommandLineOptions(argc, argv, "escape_analysis");

  if(InputFile.size() == 0)
    return -1;

  /* read in a bitcode module */
  LLVMContext   &ctx = getGlobalContext();
  SMDiagnostic  err;
  Module        *M = ParseIRFile(InputFile, err, ctx);   

  if(M == NULL)
    return -1;

  PassRegistry  &Registry = *PassRegistry::getPassRegistry();
  initializeCore(Registry);
  initializeScalarOpts(Registry);
  initializeVectorization(Registry);
  initializeIPO(Registry);
  initializeAnalysis(Registry);
  initializeIPA(Registry);
  initializeTransformUtils(Registry);
  initializeInstCombine(Registry);
  initializeInstrumentation(Registry);
  initializeTarget(Registry);

  /* instantiate a pass manager */
  PassManager   passes;

  /* instantiate an Escapes class and add to pass manager */
  passes.add(new Escapes(true));

  /* run the module through the pass manager */
  passes.run(*M);

  return 0;
}
