#include "escaped_allocas_pass.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/InstIterator.h"
#include "llvm/Instructions.h"
#include "llvm/DebugInfo.h"
#include "llvm/DIBuilder.h"
#include "llvm/IntrinsicInst.h"

using namespace llvm;
using namespace std;

Instruction *Escapes::descendValue(Value *V, set<Value*> &locals)
{
  //Is the value a pointer, and
  //Can I find that pointer in the set of local values?
  if(V->getType()->isPointerTy() && (locals.find(V) != locals.end()))
    return dyn_cast<Instruction>(V);

  if(InsertValueInst *IVI = dyn_cast<InsertValueInst>(V))
  {
    Instruction *A1 = this->descendValue(IVI->getAggregateOperand(), locals);
    Instruction *A2 = 
        this->descendValue(IVI->getInsertedValueOperand(), locals);
    
    if(A1 != NULL)
      return A1;
    
    if(A2 != NULL)
      return A2;
    
    return NULL;
  }
 
  if(SelectInst *SI = dyn_cast<SelectInst>(V))
  {
    Instruction *A1 = this->descendValue(SI->getTrueValue(), locals);
    Instruction *A2 = this->descendValue(SI->getFalseValue(), locals);
    
    if(A1 != NULL)
      return A1;
    
    if(A2 != NULL)
      return A2;
    
    return NULL;
  }

  return NULL;
}

bool Escapes::runOnFunction(Function &F) 
{
  set<Value*>       escapes;
  set<Value*>       locals;

  outs() << "Function\n";
  F.print(outs());
  outs() << "\n";

  for( inst_iterator I = inst_iterator(F), E = inst_end(F); I != E; ++I ) 
  {
    Instruction *inst = &*I;

    outs() << "Instruction:\n";
    I->print(outs());
    outs() << "\n";
    if(I->getNumUses() == 0)
      outs() << "Instruction has no uses!\n";
    outs() << "Instruction uses:\n";
    for(Value::use_iterator i = I->use_begin(), e = I->use_end(); i != e; ++i) 
    {
      i->print(outs());
      outs() << "\n";
    }
    outs() << "\n";
  }
#if 0
    //is this instruction an alloca inst?
    if( AllocaInst *AL = dyn_cast<AllocaInst>(inst) ) 
    {
      /* If an instruction is an Alloca, is it a local or an escape?
       */
#error Make a choice
      //1. 
      //locals.insert(AL);
      //2.
      //escapes.insert(AL);
    }

    //determine all of the locations in the function where a value is returned
    //and add to the escape set
    if( ReturnInst *R = dyn_cast<ReturnInst>(inst) )
    {
      //does this return instruction leak any values?
      if(Value *returnVal = R->getReturnValue()) 
      {
#error Make a choice
        //1. 
        //escapes.insert(returnVal);
        //2. 
        //locals.insert(returnVal);
      }
    }

    //determine all of the locations where a value is written to non-local 
    //memory and add to the escape set
    if( StoreInst *S = dyn_cast<StoreInst>(inst) ) 
    {
#error Make a choice
      //1.
      //escapes.insert(S->getValueOperand());
      //2.
      //escapes.insert(S);
      //3.
      //escapes.insert(S->getPointerOperand());
      //4.
      //locals.insert(S);
    }
  }
  
  
  //now, for every escape point do a check
  for(set<Value*>::iterator it = escapes.begin(); it != escapes.end(); ++it)
  {
    Value   *K = *it;
    
    if(Instruction *IK = this->descendValue(K, locals))
    {
      if(this->doPrintFails)
      {
        outs() << "In function " << F.getName() << "\n";
        outs() << "Leaking pointer to variable " << this->getVarNameFromDbg(IK);
        outs() << "\n";
      }
    }
  }
#endif

  //we never update things
  return false;
}

char Escapes::ID = 0;
