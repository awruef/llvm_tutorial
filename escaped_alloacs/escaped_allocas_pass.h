#ifndef _ESCAPED_ALLOCAS_PASS
#define _ESCAPED_ALLOCAS_PASS
#include "llvm/LLVMContext.h"
#include "llvm/DataLayout.h"
#include "llvm/DebugInfo.h"
#include "llvm/Module.h"
#include "llvm/PassManager.h"
#include "llvm/Analysis/AliasAnalysis.h"
#include "llvm/Analysis/AliasSetTracker.h"
#include "llvm/Pass.h"
#include <set>

class Escapes : public llvm::FunctionPass {
  static char                   ID;
  bool                          doPrintFails;

  llvm::Instruction *descendValue(llvm::Value *, std::set<llvm::Value*> &);
  std::string getVarNameFromDbg(llvm::Instruction *);
public:
  Escapes() : llvm::FunctionPass(ID), doPrintFails(false) { }
  Escapes(bool f) : llvm::FunctionPass(ID), doPrintFails(f) { }

  virtual bool runOnFunction(llvm::Function &);

  virtual void getAnalysisUsage(llvm::AnalysisUsage &AU) const {
    AU.addRequired<llvm::AliasAnalysis>();
    AU.setPreservesAll();   
  }

  virtual const char *getPassName() const {
      return "Escapes";
  }
};

#endif
