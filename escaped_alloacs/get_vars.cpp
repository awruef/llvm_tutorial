#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/InstIterator.h"
#include "llvm/Instructions.h"
#include "llvm/DebugInfo.h"
#include "llvm/DIBuilder.h"
#include "llvm/IntrinsicInst.h"

#include "escaped_allocas_pass.h"
#include "get_vars.h"

using namespace llvm;
using namespace std;

static const DbgValueInst *findDbgValue(const Value *V) {
  V = V->stripPointerCasts();

  if (!isa<Instruction>(V) && !isa<Argument>(V))
    return 0;

  const Function *F = NULL;
  if (const Instruction *I = dyn_cast<Instruction>(V))
    F = I->getParent()->getParent();
  else if (const Argument *A = dyn_cast<Argument>(V))
    F = A->getParent();

  for (Function::const_iterator FI = F->begin(), FE = F->end(); FI != FE; ++FI)
    for (BasicBlock::const_iterator BI = (*FI).begin(), BE = (*FI).end();
         BI != BE; ++BI)
      if (const DbgValueInst *DVI = dyn_cast<DbgValueInst>(BI))
        if (DVI->getValue() == V)
          return DVI;

  return 0;
}

static const DbgDeclareInst *findDbgDeclare(const Value *V) {
  V = V->stripPointerCasts();

  if (!isa<Instruction>(V) && !isa<Argument>(V))
    return 0;

  const Function *F = NULL;
  if (const Instruction *I = dyn_cast<Instruction>(V))
    F = I->getParent()->getParent();
  else if (const Argument *A = dyn_cast<Argument>(V))
    F = A->getParent();

  for (Function::const_iterator FI = F->begin(), FE = F->end(); FI != FE; ++FI)
    for (BasicBlock::const_iterator BI = (*FI).begin(), BE = (*FI).end();
         BI != BE; ++BI)
      if (const DbgDeclareInst *DDI = dyn_cast<DbgDeclareInst>(BI))
        if (DDI->getAddress() == V)
          return DDI;

  return 0;
}

string Escapes::getVarNameFromDbg(Instruction *I)
{
  string    var = "";
  const DbgValueInst    *DVI = findDbgValue(I);

  if(DVI)
  {
    DIVariable DebugVar(cast<MDNode>(DVI->getVariable()));

    var = DebugVar.getName();
  }
  else
  {
    const DbgDeclareInst    *DDI = findDbgDeclare(I);

    if(DDI)
    {
      DIVariable    DebugVar(cast<MDNode>(DDI->getVariable()));

      var = DebugVar.getName();
    }
  }

  return var;
}
