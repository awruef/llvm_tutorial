#include <stdint.h>

struct stuff {
  uint32_t   a;
  uint32_t   b;
  uint32_t   c;
};

int blah2(struct stuff j[100])
{
    return j[1].a*j[1].b*j[1].c;
}
