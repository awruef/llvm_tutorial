#!/bin/bash
clang -c -emit-llvm -o $1.bc $1.c
opt -mem2reg -o $1_m2r.bc $1.bc
opt -O1 -o $1.bc $1.bc
llvm-dis $1_m2r.bc
llvm-dis $1.bc
