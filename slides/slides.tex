\documentclass{beamer}\usetheme{default}

\usepackage{url}
\usepackage{hyperref}
\usepackage[override]{cmtt}
\usepackage{graphicx}

\title[LLVM Tutorial]{Using LLVM For Program Transformation}
\author[A. Ruef]{Andrew Ruef}
\date[May 2013]{May 14, 2013}
\begin{document}

\begin{frame}[plain]
  \titlepage
\end{frame}

\section{Introduction}
\begin{frame}[t]{Components of LLVM}

\begin{figure}[htb]
\centering
\includegraphics[scale=0.45]{../../slides/LLVM_high_level.png}
\end{figure}

\begin{enumerate}
\item Mid-level compiler Intermediate Representation (IR)
\item C/C++ compiler frontend (clang)
\item Target-specific (X86, ARM, Blackfin, etc...) code generators
\item Divide between 'clang' and 'LLVM' 
\item Clang is a C/C++ compiler with an LLVM backend
\item LLVM is 'everything else' in a compiler
\end{enumerate}
\end{frame}

\begin{frame}[t]{Project Documentation}
\begin{enumerate}
\item Good documentation exists online
\item http://www.llvm.org/docs
\item Documentation covers many angles of the LLVM project
\item Programmers manual details finer points of the LLVM C++ API and environment
\item Language reference is ultimate source for language details and semantics
\item Relatively responsive IRC channel on OFTC
\item Pretty active and responsive mailing list
\end{enumerate}
\end{frame}

\begin{frame}[t]{Todays Adgenda}
\begin{enumerate}
\item We'll talk about existing LLVM tools
\item We'll do a few demos using those tools
\item We'll talk about how to build tools on top of LLVM
\item We'll build two analyses tools 
\item We'll look at a program re-writing tool
\end{enumerate}
\end{frame}

\begin{frame}[t]{Where we're going: Brief lab}
\begin{enumerate}
\item clang - C language frontend, translates C into unoptimized LLVM bitcode
\item opt - Analyze and transform LLVM bitcode 
\item llc - Code generator for LLVM bitcode to native code
\end{enumerate}
\end{frame}

\begin{frame}[t]{Command lines}
\begin{enumerate}
\item clang -c -emit-llvm -o test.bc test.c
\item opt -O1 -o test.bc test.bc
\item llc -o test.s test.bc
\item gcc -o test test.s
\end{enumerate}
\end{frame}

\begin{frame}[t]{What did we get? What just happened?}
\begin{enumerate}
\item Full stage of translation of C program to executable program
\item At each stage we can look at what the compiler infrastructure is doing
\item C to unoptimized bitcode
\item Optimized bitcode, can dump every step of this transformation
\item Machine code
\item Executable
\item Very good blog post on life of an LLVM instruction
\end{enumerate}
\end{frame}

\section{LLVM}

\subsection{LLVM IR}
\begin{frame}[t]{LLVM IR}
\begin{enumerate}
\item Language allows for expression of computation
\item Instructions produce unique values 
\item Statements take the form of \%5 = add nsw i32 \%3, \%4
\item \%N - refers to value N
\item add - a binary instruction that performs arithmetic
\item nsw - no signed wrap
\item \%3, \%4 - value argument list, other values defined previously
\item The language is Static Single Assignment 
\item Values defined by statements are never re-defined
\end{enumerate}
\end{frame}

\begin{frame}[t]{Hierarchy of the Language}

\begin{figure}[htb]
\centering
\includegraphics[scale=0.75]{../../slides/LLVM_module.png}
\end{figure}

\begin{enumerate}
\item A compilation unit is a Module, contains functions
\item A function is a Function, contains basic blocks
\item A basic block is a BasicBlock, contains instructions
\item An instruction is an Instruction
\item Instructions can contain operands, each is a Value
\item All of the above, except Module, is a Value
\end{enumerate}
\end{frame}

\begin{frame}[t]{Types}
\begin{enumerate}
\item No implicit casting in LLVM IR, all values must be explicitly converted
\item All values have a static type
\item Integers are specified at any bitwidth, i1, i2, i3, ..., i32, ..., i198
\item Floating point types 
\item Derived types specify arrays, vectors, functions, pointers, structures
\item Structures have types like \{i32, i32, i8\}
\item Pointers have types like "pointer to i32" 
\end{enumerate}
\end{frame}

\begin{frame}[t]{Note on Integer Types}
\begin{enumerate}
\item There are no signed or unsigned integer values in the LLVM typesystem
\item LLVM views integer values as bit vectors and operates as a bit vector machine
\item Frontends destroy signed/unsigned information when converting to LLVM
\item Really, C programmers destroy signed/unsigned information...
\item Research prototypes that analyzes integer wrapping in LLVM IR (http://code.google.com/p/wrapped-intervals/)
\end{enumerate}
\end{frame}

\begin{frame}[t]{Memory Model}
\begin{enumerate}
\item LLVM IR has a low level view of memory, it is just a key $\rightarrow$ value map
\item Keys are pointer values
\item Values stored in LLVM memory must be integers, floating point, pointers, vectors, structures, or arrays
\item LLVM has more complicated semantics for memory models specified by C/C++11
\item LLVM also has a concept of creating function-local memory via alloca
\end{enumerate}
\end{frame}

\begin{frame}[t]{The Module}
\begin{enumerate}
\item Highest level 
\item Contains things of global visibility
\item Global variables
\item Functions
\end{enumerate}
\end{frame}

\begin{frame}[t]{The Function}
\begin{enumerate}
\item Name
\item Argument list
\item Return type
\item Calling convention
\item Extends from GlobalValue, so also has properties of linkage visability
\end{enumerate}
\end{frame}

\begin{frame}[t]{The BasicBlock}
\begin{enumerate}
\item Contains a list of Instructions 
\item All BasicBlocks must end in a TerminatorInst
\item BasicBlocks descend from values, and are used as values in branching instructions
\end{enumerate}
\end{frame}

\begin{frame}[t]{The Instruction}
\begin{enumerate}
\item Terminator instructions
\item Binary instructions
\item Bitwise instructions
\item Aggregate instructions
\item Memory instructions
\item Type conversion instructions
\item Control and misc instructions
\end{enumerate}
\end{frame}

\begin{frame}{Language By Example}
\begin{figure}[htb]
\centering
\includegraphics[scale=0.5]{../../slides/cfg_fib.png}
\end{figure}
\end{frame}

\begin{frame}{Statement by Statement}
\begin{enumerate}
\item icmp ult i32 \%n, 2 - integer compare, unsigned less than
\item br - conditional branch, on condition produced by icmp
\item add i32 \%n, -1 - add value 'n' and -1 
\item call (i32 \%3) - call with value produced by previous add
\item ret \%7 - return value \%7
\end{enumerate}
\end{frame}

\begin{frame}{Language By Example Part 2}
\begin{figure}[htb]
\centering
\includegraphics[scale=0.36]{../../slides/cfg_xform.png}
\end{figure}
\end{frame}

\begin{frame}{Again, Statement by Statement}
\begin{enumerate}
\item New instructions
\item phi ($\phi$) - select value based on taken branch
\item getelemenptr - compute offset into nth field of structure type Foo. 'inbounds' notation
\item shl - shift left
\item load - read value from memory 
\end{enumerate}
\end{frame}

%\begin{frame}[t]{Terminator Instructions}
%\begin{enumerate}
%\item Terminator instructions end blocks, sometimes with a branch, partial list
%\item ret - return
%\item br - branch
%\item switch 
%\item unreachable
%\end{enumerate}
%\end{frame}

%\begin{frame}[t]{Binary Instructions}
%\begin{enumerate}
%\item Integer and floating point binary operations
%\item add, fadd
%\item sub, fsub
%\item mul, fmul
%\item udiv, sdiv, fdiv
%\item urem, srem, frem
%\end{enumerate}
%\end{frame}

%\begin{frame}[t]{Bitwise Instructions}
%\begin{enumerate}
%\item Bit-level binary operations on integer values
%\item shl, lshr, ashr - shift left, logical shift right, arithmetic shift right
%\item and, or, xor
%\end{enumerate}
%\end{frame}

%\begin{frame}[t]{Aggregate Instructions}
%\begin{enumerate}
%\item Used for manipulating aggregate types, i.e. struct types in C
%\item extractvalue
%\item insertvalue
%\end{enumerate}
%\end{frame}

%\begin{frame}[t]{Memory Instructions}
%\begin{enumerate}
%\item Instructions that operate on memory
%\item alloca - allocate local storage
%\item load - load from memory
%\item store - store to memory
%\item getelemenptr - compute pointer to element in aggregate data structure
%\end{enumerate}
%\end{frame}

%\begin{frame}[t]{Conversion Instructions}
%\begin{enumerate}
%\item Perform type conversion / casting of values
%\item trunc - Change from higher to lower bitwidth of integer values
%\item zext/sext - Extend from lower to higher, with sign or zero extend 
%\item fptrunc / fpext - same as above on floating point types
%\item fptoui / fptosi / uitofp / sitofp - Convert floating point to integer and vice versa, signed and unsigned
%\item inttoptr / ptrtoint - Convert to and from integer and pointer types
%\item bitcast - Change type without changing any value
%\end{enumerate}
%\end{frame}

%\begin{frame}[t]{Control Instructions}
%\begin{enumerate}
%\item icmp / fcmp - boolean value based on integer or floating point compare
%\item eq, ne, ugt, uge, ult, ule, sgt, sge, slt, sle
%\item phi - $\phi$ instruction to select a value based on edge taken
%\item select - "if-then-else" as a single statement
%\item call - call a subroutine
%\end{enumerate}
%\end{frame}

\begin{frame}[t]{Static Single Assignment}
\begin{enumerate}
\item LLVM contains a pass to promote variable-using functions to value-using functions
\item Once transformed by this pass, an LLVM module is in SSA form
\item Most LLVM analyses and transformations expect to operate on SSA form IR
\item SSA form IR allows for Def-Use and Use-Def chain analysis
\item SSA complicates loop structure or conditional assignments
\end{enumerate}
\end{frame}

\begin{frame}[t]{The $\phi$-Node}
\begin{enumerate}
\item To support conditional assignments, we introduce an imaginary function
\item $\phi$ defines a value and accepts a list of tuples as an argument
\item Each tuple is a (BasicBlock $*$ Value) 
\item Interpreted as, define value to value conditionally on previous basic block
\end{enumerate}
\end{frame}

\begin{frame}[t]{Well-Formed LLVM}
\begin{enumerate}
\item There are specific rules as to what constitutes "Well-Formed" LLVM
\item $\phi$ nodes dominate their uses
\item Instruction arguments are defined before use
\item All blocks end in a terminator
\item All branch targets are defined values
\item There is an automatic verification pass that ensures IR is well formed
\end{enumerate}
\end{frame}

\subsection{Lab: Non-Constant Format String}

\begin{frame}[t]{Non-Constant Format String}
\begin{enumerate}
\item Any time the first parameter to printf, sprintf is non-constant, potential for security badness
\item Can we statically detect this?
\end{enumerate}
\end{frame}

\begin{frame}[t]{Algorithm For Detection}
\begin{enumerate}
\item Visit every call instruction in the program
\item Ask if the call instruction is a format-string accepting routine
\item If it is, retrieve the first parameter to this call instruction
\item If the first parameter is not a constant global, raise an alert
\end{enumerate}
\end{frame}

\begin{frame}[t]{Structure of Provided Driver}
\begin{enumerate}
\item Very basic driver that uses a PassManager
\item Reads in LLVM bitcode and runs the VarPrintf pass on it
\item Produce bitcode file using clang -c -emit-llvm
\item Use opt to apply some optimizations to the bitcode (-O1) before passing to driver
\item Driver seems cludgy but this is easier than integrating with opt
\item The pass can be taken out of this project and placed into opt later
\end{enumerate}
\end{frame}

\subsection{API}
\begin{frame}[t]{Value Hierarchy}
\begin{enumerate}
%insert screenshot of Value hierarchy from older training
\item Value has a very rich class hierarchy
\item The API allows the manipulation of every value within a Module
\item Anything is possible - rewrite, add, remove
\end{enumerate}
\end{frame}

\begin{frame}[t]{Everything From Value}
\begin{enumerate}
\item Every item contained in a Module inherits from Value
\item This allows for some useful APIs and concepts
\item Def-Use / Use-Def iteration all works on Value iterators
\item Values can be replaced with other Values

\begin{figure}[htb]
\centering
\includegraphics[scale=0.19]{../../slides/value.png}
\end{figure}

\end{enumerate}
\end{frame}

\begin{frame}[t]{LLVM Context}
\begin{enumerate}
\item Many APIs take a pointer/reference to an LLVMContext object
\item These can normally be derived from a Value
\item There is a globally visible getGlobalContext as well
\item LLVMContext should always be the same across code that interacts with the same Module/Values
\item Type objects are immutable in LLVM, so comparisons of equality for Types is done by pointer comparisons
\item Those pointer comparisons are only valid within the same LLVM context
\end{enumerate}
\end{frame}

\begin{frame}[t]{LLVM And RTTI}
\begin{enumerate}
\item LLVM uses its own RTTI implementation
\item isa\textless T\textgreater - true or false if pointer/reference is of type T
\item cast\textless T\textgreater - "checked cast", asserts on failure if not type T
\item dyn\_cast\textless T\textgreater - unchecked cast, null if not type T
\item The project advises you to not use big chains of these to approximate match
\item Instead they give you Visitor patterns 
\item You might find these insufficient (or distasteful)
\end{enumerate}
\end{frame}

\begin{frame}[t]{Common Patterns}
\begin{enumerate}
\item "Iterate over BasicBlock in a Function" - use begin(), end() of Function
\item "Iterate over Instructions in a Function" - use inst\_iterator
\item "Iterate over def-use chains" - use use\_begin(), use\_end()
\end{enumerate}
\end{frame}

\begin{frame}[t]{InstVisitor}
\begin{enumerate}
\item Pattern to avoid giant blocks of if(T *n = dyn\_cast\textless T\textgreater (foo))
\item Inherit from InstVisitor class and define visitTInst method
\item Could work for your purposes, could confuse control flow more
\end{enumerate}
\end{frame}

\begin{frame}[t]{Inclduing LLVM In Your Project}
\begin{enumerate}
\item llvm-config - executable that will provide useful info about installed llvm
\item Provide paths to headers, library files, etc
\item If LLVM is built with CMake, it will add a FindLLVM.cmake to your /usr/share
\item Compiling your code -fno-rtti will probably be required
\item If you compile LLVM yourself, you can pass LLVM\_REQUIRES\_RTTI to cmake
\item Needed if combining say, boost and llvm
\end{enumerate}
\end{frame}

\section{Transformations}

\subsection{Passes}

\begin{frame}[t]{Passes}
\begin{enumerate}
\item In the previous lab, we wrote a pass
\item Compilation is the act of passing over and analyzing/transforming IR
\item Most things that happen in LLVM happen in the context of a pass
\item Passes can have complicated actions
\item Many sophisticated analyses (alias) and transformations (LICM) implemented in LLVM
\end{enumerate}
\end{frame}

\begin{frame}[t]{Pass Dependencies}
\begin{enumerate}
\item Passes can consume output of other (analysis) passes
\item Passes note their dependencies on other passes
\item PassManager figures out who runs before who 
\item Each Pass returns a bool, passes run until convergance
\end{enumerate}
\end{frame}

\begin{frame}[t]{Pass Manager}
\begin{enumerate}
\item PassManager performs dependency maintenance of passes
\item Note that PassManager invocations can be multi-threaded!
\item PassManager also performs optimizations of pass ordering
\item PassManager defines different kinds of Passes that can be run
\item ModulePass - Run on entire module
\item FunctionPass - Run on each function individually
\item BasicBlockPass - Run on each basic block individually
\item CallGraphSCC Pass - Traverse call graph of module
\end{enumerate}
\end{frame}

\begin{frame}[t]{Pass Rules}
\begin{enumerate}
\item Generally, transformation passes should not carry information about transformed programs across pass invocations
\item Analysis passes are expected to carry information however
\item Some pass types have exceptions to these rules
\end{enumerate}
\end{frame}

\subsection{Lab: Escape Analysis}

\begin{frame}[t]{Escape Analysis}
\begin{enumerate}
\item If a variable is allocated on the local stack, a pointer to that variable should not outlive the stack
\item This could happen if a pointer to a local is returned or assigned to a global
\item clang currently includes a check for this, but the check is syntax driven and easily fooled
\end{enumerate}
\end{frame}

\begin{frame}[t]{Algorithm For Escape Analysis}
\begin{enumerate}
\item Populate a set of values that escape the function via return or store
\item Traverse the set checking for alloca-ed values in the Values descending from the escapes
\end{enumerate}
\end{frame}

\begin{frame}[t]{Structure of Provided Driver}
\begin{enumerate}
\item Driver is laid out similarly before
\item Probably beneficial to -O1 every bitcode input to your analysis
\item Not needed to be sound/conservative, but helps remove false positives
\end{enumerate}
\end{frame}

\subsection{Dynamic Analysis Tactics}

\begin{frame}[t]{Some Analysis Tactics}
\begin{enumerate}
\item Instruments can be statically inserted into LLVM IR, these instruments have access to all the values that the program itself has 
\item Monitors can make program influencing decisions at runtime given awareness and insertion at compile time 
\item Inspiration: Check for integer overflow conditions at run-time
\item Inspiration: Check for threads accessing private data of other threads
\item Inspiration: Check for memory writes outside the bounds of allocated objects
\end{enumerate}
\end{frame}

\end{document}
