typedef struct _Rec {
    int K;
    int J;
    int V;
    int *A;
} Rec;

Rec foo(int a, int b) {
    Rec r;
    int j = a+b;

    r.K = a;
    r.J = b;
    r.V = 0;
    r.A = &j;

    return r;
}
