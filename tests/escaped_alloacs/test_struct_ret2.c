#include <stdlib.h>

typedef struct _Rec {
    int B;
    int C;
    int *A;
} Rec;

Rec foo(int a, int b) {
    Rec r;
    int j = a+b;

    r.B = a;
    r.C = b;
    if( rand() % 2 == 0) {
        r.A = &j;
    } else {
        r.A = NULL;
    }

    return r;
}
