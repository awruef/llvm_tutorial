#include <stdio.h>

const char bar_a[] = "bar\n";
char baz_a[] = "baz\n";

void foo(void) {
  printf("foo\n");
}

void bar(void) {
  printf(bar_a);
}

void baz(void) {
  printf(baz_a);
}
