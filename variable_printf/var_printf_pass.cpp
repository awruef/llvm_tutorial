#include "var_printf_pass.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/ADT/StringSwitch.h"
#include "llvm/Support/InstVisitor.h"
#include "llvm/Instructions.h"

using namespace llvm;
using namespace std;

bool VarPrintf::runOnFunction(Function &F)
{
  F.dump();
  for(inst_iterator I = inst_begin(F), E = inst_end(F); I != E; ++I)
  {
    Instruction *inst = &*I;
    /* Which Value should we be inspecting?
     */
#error Make a choice
    User  *C = NULL;
    // 1. CallInst
    //if( CallInst *C = dyn_cast<CallInst>(inst) ) 
    // 2. Function
    //if( Function *C = dyn_cast<Function>(inst) )
    // 3. User
    if( (C = dyn_cast<User>(inst)) != NULL )
    {
      /* Which of these will work? reliably for our purposes?
       */
      Value *FU = NULL;
      // 1. 
      //Value *FU = NULL;
      //if( (FU = C->getCalledFunction()) != NULL )
      // 2.
      //Function *FU = NULL;
      //if( (FU = C->getCalledFunction()) != NULL )
      // 3.
      //CallInst *FU = NULL;
      //if( (FU = C->getCalledFunction()) != NULL )
      {
        string  s = FU->getName();
        bool    isVar = StringSwitch<bool>(s)
            .Case("printf", true)
            .Case("fprintf", true)
            .Default(false);

        if( isVar )
        {
          /* Which of these do we want?
           */
          Value *firstArg = NULL;
#error Make a choice
          // 1. 
          //firstArg = C->getArgOperand(0)->stripPointerCasts();
          // 2.
          //firstArg = C->getArgOperand(0);
          /* Uncomment this for debugging!
           */
          //firstArg->dump();
          //This should be a constant global
          GlobalValue   *GV;
          bool          reportErr = false;
          if((GV = dyn_cast<GlobalValue>(firstArg)) == NULL)
          {
            reportErr = true;
          } 
          else
          {
            if(GlobalVariable *GVN = dyn_cast<GlobalVariable>(firstArg))
            {
              if(GVN->isConstant() == false)
                reportErr = true;
            }
          }

          if(reportErr)
          {
            errs() << "Non-constant in function ";
            errs() << F.getName();
            errs() << "\n";
          }
        }
      }
    }
  }

  return false;
}

struct VarPrintfVisitor : public InstVisitor<VarPrintfVisitor> {

    VarPrintfVisitor() { }

    void visitCallInst(CallInst &C);
};

void VarPrintfVisitor::visitCallInst(CallInst &C)
{
  if( Function *FU = C.getCalledFunction() )
  {
    string  s = FU->getName();
    bool    isVar = StringSwitch<bool>(s)
        .Case("printf", true)
        .Default(false);

    if( isVar )
    {
      Value *firstArg = C.getArgOperand(0)->stripPointerCasts();
      //This should be a constant global
      GlobalValue   *GV;
      if((GV = dyn_cast<GlobalValue>(firstArg)) == NULL)
      {
        errs() << "Non-constant in function ";
        //      inst     block      function    name
        errs() << C.getParent()->getParent()->getName();
        errs() << "\n";
      }
    }
  }

  return;
}

bool VarPrintfV::runOnFunction(Function &F)
{
  VarPrintfVisitor  VS;

  VS.visit(F);

  return false;
}


char VarPrintf::ID = 0;
char VarPrintfV::ID = 0;
