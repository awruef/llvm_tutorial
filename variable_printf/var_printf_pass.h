#ifndef _ESCAPED_ALLOCAS_PASS
#define _ESCAPED_ALLOCAS_PASS
#include "llvm/LLVMContext.h"
#include "llvm/DataLayout.h"
#include "llvm/DebugInfo.h"
#include "llvm/Module.h"
#include "llvm/PassManager.h"
#include "llvm/Pass.h"
#include "llvm/Support/InstIterator.h"
#include <set>

class VarPrintf : public llvm::FunctionPass {
  static char                   ID;

public:
  VarPrintf() : llvm::FunctionPass(ID) { }

  virtual bool runOnFunction(llvm::Function &);
};

class VarPrintfV : public llvm::FunctionPass {
  static char                   ID;

public:
  VarPrintfV() : llvm::FunctionPass(ID) { }

  virtual bool runOnFunction(llvm::Function &);
};

#endif
