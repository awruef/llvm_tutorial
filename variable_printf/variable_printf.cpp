#include "llvm/LLVMContext.h"
#include "llvm/DataLayout.h"
#include "llvm/DebugInfo.h"
#include "llvm/Module.h"
#include "llvm/PassManager.h"
#include "llvm/Support/IRReader.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/LinkAllPasses.h"
#include "llvm/LinkAllVMCore.h"
#include "var_printf_pass.h"
#include <memory>
#include <algorithm>
using namespace llvm;
using namespace std;

static cl::opt<string>
InputFile(cl::Positional, cl::desc("input"));

int main(int argc, char *argv[])
{
  cl::ParseCommandLineOptions(argc, argv, "variable_printf");

  if(InputFile.size() == 0)
    return -1;

  LLVMContext   &ctx = getGlobalContext();
  SMDiagnostic  err;
  Module        *M = ParseIRFile(InputFile, err, ctx);

  if(M == NULL)
    return -1;

  PassManager   passes;

  passes.add(new VarPrintf());
  passes.run(*M);

  return 0;
}
